package cn.wk.picture.activity;

import android.content.Intent;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;

import wk.cn.base.BaseActivity;
import wk.cn.photodemo.R;

/**
 * .class
 * company: m6go.cn
 * User: wangkai(wk343321@126.com)
 * Date: 2015-12-12
 * Time: 12:16
 * belong: android group
 * FIXME
 */
public class CameraActivity extends BaseActivity implements View.OnClickListener {

    private Button selectPhoto;
    private Button takePhoto;

    @Override
    protected void initView() {
        setContentView(R.layout.activity_camera);

        selectPhoto = (Button) findViewById(R.id.btn_select_photo);
        takePhoto = (Button) findViewById(R.id.btn_take_photo);
    }

    @Override
    protected void initData() {
        selectPhoto.setOnClickListener(this);
        takePhoto.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btn_select_photo:
                //选择相册

                break;

            case R.id.btn_take_photo:
                //系统拍照
                intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//                intent.putExtra()

                break;
        }
    }
}
