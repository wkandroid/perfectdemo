package cn.wk.base.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;

import cn.wk.base.R;

/**
 * .class
 * company: m6go.cn
 * User: wangkai(wk343321@126.com)
 * Date: 2015-12-12
 * Time: 12:16
 * belong: android group
 * FIXME
 */
public abstract class BaseActivity extends Activity {

    /**
     * 标题左键
     */
    private RelativeLayout _projectLeftKey;
    /**
     * 标题容器
     */
    private FrameLayout _container;
    /**
     * 标题title
     */
    private TextView _projectCenterText;
    /**
     * 布局填充器
     */
    private LayoutInflater _inflater;
    /**
     * 软键盘管理者
     */
    private InputMethodManager _inputMethodManager;

    /**
     * 当前页面的上下文
     */
    protected Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.base);
        context = this;
        _inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        _inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        _container = (FrameLayout) findViewById(R.id.fl_container);
        _projectLeftKey = (RelativeLayout) findViewById(R.id.project_left_back);
        _projectCenterText = (TextView) findViewById(R.id.project_center_text);

        initView();
        setListener();
        initData();
    }

    /**
     * 子activity实现布局的操作
     */
    protected abstract void initView();

    /**
     * 设置监听器
     */
    protected abstract void setListener();

    /**
     * 子activity实现数据的操作
     */
    protected abstract void initData();

    /**
     * 子activity添加布局
     *
     * @param layoutId
     */
    public void setContentView(int layoutId) {
        _container.addView(_inflater.inflate(layoutId, null));
    }


    /**
     * 左键返回
     */
    protected void _leftKeyBack() {
        _projectLeftKey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    /**
     * 子activity实现标题
     *
     * @param titleText
     */
    protected void _centerKeyText(String titleText) {
        _projectCenterText.setText(titleText);
    }


    /**
     * 获取输入法打开的状态
     */
    protected boolean _softKeyboardIsOpen() {
        return _inputMethodManager.isActive();
    }

    /**
     * 调用系统默认的输入法
     */
    protected void _startSystemInput() {
        _inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * 强制隐藏软键盘
     */
    protected void _hiddenSoftKeyboard(View view) {
        _inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * 软键盘显示便隐藏，隐藏便显示
     */
    protected void _hiddenSoftKeyboard() {
        _inputMethodManager.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }


    /**
     * 判断sd卡是否处于挂载状态
     *
     * @return
     */
    protected boolean isSdcardExits() {

        return TextUtils.equals(Environment.MEDIA_MOUNTED, Environment.getExternalStorageState());
    }


    /**
     * 判断sdcard可用控件是否大于3M
     *
     * @return
     */
    protected boolean isSdcardEnable() {

        int freeSpace = Integer.parseInt(Formatter.formatFileSize(this, Environment.getExternalStorageDirectory().getFreeSpace()));

        if (3 > freeSpace) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 开启页面
     *
     * @param activity   需要打开的activity
     * @param extras     需要传递的参数
     * @param isFinished 是否结束当前界面
     * @param <T>
     */
    protected <T> void _startActivity(Activity activity, HashMap<String, T> extras, boolean isFinished) {
        Intent intent = new Intent();
        intent.setClass(this, activity.getClass());
        if (null != extras && extras.size() > 0) {
            Iterator<String> iterator = extras.keySet().iterator();

            while (iterator.hasNext()) {
                String key = iterator.next();
                T value = extras.get(key);

                if (value instanceof String) {
                    intent.putExtra(key, (String) value);
                } else if (value instanceof Class) {
                    intent.putExtra(key, (Serializable) value);
                }
            }
        }
        startActivity(intent);

        if (isFinished) {
            finish();
        }
    }

    /**
     * 开启页面
     *
     * @param activity  需要打开的activity
     * @param extras    需要传递的参数
     * @param startCode 打开页面的请求码
     * @param <T>
     */
    protected <T> void _startActivity(Activity activity, HashMap<String, T> extras, int startCode) {
        Intent intent = new Intent();
        intent.setClass(this, activity.getClass());
        if (null != extras && extras.size() > 0) {
            Iterator<String> iterator = extras.keySet().iterator();

            while (iterator.hasNext()) {
                String key = iterator.next();
                T value = extras.get(key);

                if (value instanceof String) {
                    intent.putExtra(key, (String) value);
                } else if (value instanceof Class) {
                    intent.putExtra(key, (Serializable) value);
                }
            }
        }
        startActivityForResult(intent, startCode);
    }

}
