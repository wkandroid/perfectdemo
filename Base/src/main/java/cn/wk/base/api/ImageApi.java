package cn.wk.base.api;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

/**
 * .class
 * company: m6go.cn
 * User: wangkai(wk343321@126.com)
 * Date: 2015-12-12
 * Time: 12:13
 * belong: android group
 * FIXME
 */
public class ImageApi {


    /**
     * 图片合成
     *
     * @param src
     * @return
     */
    private Bitmap createBitmap(Bitmap src, Bitmap watermark) {
        if (src == null) {
            return null;
        }
        int w = src.getWidth();
        int h = src.getHeight();
        int ww = watermark.getWidth();
        int wh = watermark.getHeight();
//        create the new blank bitmap
        Bitmap newb = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);//创建一个新的和SRC长度宽度一样的位图
        Canvas cv = new Canvas(newb);
//        draw src into
        cv.drawBitmap(src, 0, 0, null);//在 0，0坐标开始画入src
//        draw watermark into
        cv.drawBitmap(watermark, w - ww + 5, h - wh + 5, null);//在src的右下角画入水印
//        save all clip
        cv.save(Canvas.ALL_SAVE_FLAG);//保存
//        store
        cv.restore();//存储
        return newb;
    }


//    图片圆角

    /**
     * 图片圆角
     *
     * @param bitmap
     * @return
     */
    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = 12;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }


//    图片缩放、翻转和旋转


    /**
     * 缩放、翻转和旋转图片
     *
     * @param bmpOrg
     * @param rotate
     * @return
     */
    private Bitmap gerZoomRotateBitmap(
            Bitmap bmpOrg, int rotate) {
//        获取图片的原始的大小
        int width = bmpOrg.getWidth();
        int height = bmpOrg.getHeight();

        int newWidth = 300;
        int newheight = 300;
//        定义缩放的高和宽的比例、
        float sw = ((float) newWidth) / width;
        float sh = ((float) newheight) / height;
//        创建操作图片的用的Matrix对象
        android.graphics.Matrix matrix = new android.graphics.Matrix();
//        缩放翻转图片的动作
//        sw sh的绝对值为绽放宽高的比例，sw为负数表示X方向翻转，sh为负数表示Y方向翻转
        matrix.postScale(sw, sh);
//        旋转30 *
        matrix.postRotate(rotate);
//        创建一个新的图片
        Bitmap resizeBitmap = Bitmap
                .createBitmap(bmpOrg, 0, 0, width, height, matrix, true);
        return resizeBitmap;
    }

}
