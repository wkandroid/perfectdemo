package cn.wk.base;

import android.app.Activity;
import android.app.Application;
import android.os.Looper;
import android.text.TextUtils;

import java.util.ArrayList;

import cn.wk.base.activity.BaseActivity;

/**
 * .class
 * company: m6go.cn
 * User: wangkai(wk343321@126.com)
 * Date: 2015-12-12
 * Time: 16:47
 * belong: android group
 * FIXME
 */
public class App extends Application {

    /**
     * _mContext应用程序上下文
     */
    private App _mApp;

    /**
     * 主线程的looper轮询器
     */
    private Looper _mLopper;

    /**
     * 主线程
     */
    private Thread _mThread;

    /**
     * activity管理
     */
    private ArrayList<BaseActivity> _mActivitys = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();

        _mApp = this;
        _mLopper = getMainLooper();
        _mThread = _mLopper.getThread();
    }


    /**
     * 退出应用程序的时候主动释放
     */
    @Override
    public void onTerminate() {
        super.onTerminate();
        for (BaseActivity activity : _mActivitys) {
            activity.finish();
        }
    }

    /**
     * 获取App对象
     *
     * @return
     */
    public App _getApp() {
        return _mApp;
    }

    /**
     * 将activity统一添加到管理集合中去
     *
     * @param baseActivity
     */
    public void _addActivitys(BaseActivity baseActivity) {
        for (Activity activity : _mActivitys) {
            if (TextUtils.equals(activity.getClass().getName().toString(), baseActivity.getClass().getName().toString())) {
                _mActivitys.remove(activity);
                break;
            }
        }

        _mActivitys.add(baseActivity);
    }
}
